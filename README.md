This file is based on an idea from Jens Sogaard.
I've added recursive block searching to get fields from nested blocks

Inlcude the file in your functions that need access to block fields.

Instantiate a new helper with the Block ID and Post ID:
    $blockHelper = new BlockHelper($block_id, $post_id);

Then get the ACF block fields by calling the getBlockFields method:
    $blockFields = blockHelper->getBlockFields();

Access your field data as follows:
    $blockFields['field_name'];

