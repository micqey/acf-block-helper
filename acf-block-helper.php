<?php
/**
 * ACF Block Helper class based on code by Jens Sogaard  
 * Added recursion to get ACF Fields from nested blocks
 * @author Michael Croteau <michael@crowtoes.com>
 */
class BlockHelper {

	var $block_id;
	var $post_id;

	function __construct( string $block_id, int $post_id ) {
		$this->block_id = $block_id;
		$this->post_id = $post_id;

	}
	public function getBlockFields() {
		$post = get_post( $this->post_id );

		if (! $post ) { return false; }

		$blocks = parse_blocks( $post->post_content );

		if ($blocks) {
			$iterator  = new RecursiveArrayIterator( $blocks );
			$recursive = new RecursiveIteratorIterator(
				$iterator,
				RecursiveIteratorIterator::SELF_FIRST
			);
			foreach ( $recursive as $key => $value ) {
				if ( isset($value['attrs']) && isset($value['attrs']['id']) ){
					if ( $value['attrs']['id'] === $this->block_id ) {
						acf_setup_meta( $value['attrs']['data'], $value['attrs']['id'], true );
						acf_reset_meta( $value['attrs']['id'] );
						return $value['attrs']['data'];
					}
				}
			}
		}
		return false;

	}

}